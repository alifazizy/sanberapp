import React from 'react';

import {
 View,
 Text,
 StyleSheet,
 FlatList,
 Image,
 TouchableOpacity,
 Dimensions,
 TextInput,
 Button,
} from 'react-native';

import data from './data.json';

import ListItem from '../components/ListItem';

const DEVICE = Dimensions.get('window');

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchText: '',
      totalPrice: 0,
      data,
    };
  }

  currencyFormat(num) {
    return `Rp ${num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')}`;
  }

  updatePrice(price) {
    // ? #Soal Bonus (10 poin)
    // ? Buatlah teks 'Total Harga' yang akan bertambah setiap kali salah satu barang/item di klik/tekan.
    // ? Di sini, buat fungsi untuk menambahkan nilai dari state.totalPrice dan ditampilkan pada 'Total Harga'.

    // Kode di sini
  }


  render() {
    console.log(data);
    return (
      <View style={styles.container}>
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>
              Hai,
              {'\n'}
              {/* //? #Soal 1 Tambahan, Simpan userName yang dikirim dari halaman Login pada komponen Text di bawah ini */}
              <Text style={styles.headerText}>tempat userName</Text>
            </Text>

            {/* //? #Soal Bonus, simpan Total Harga dan state.totalPrice di komponen Text di bawah ini */}
            <Text style={{ textAlign: 'right' }}>
              Total Harga
              {'\n'}
              <Text style={styles.headerText}>tempat total harga</Text>
            </Text>
          </View>
          <View />
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder="Cari barang.."
            onChangeText={((searchText) => this.setState({ searchText }))}
          />
        </View>

        {/*
        //? #Soal No 2 (15 poin)
        //? Buatlah 1 komponen FlatList dengan input berasal dari data.json
        //? dan pada prop renderItem menggunakan komponen ListItem -- ada di bawah --
        //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal)

        // Lanjutkan di bawah ini!
        */}
        <FlatList
          data={data}
          renderItem={({ item }) => <ListItem {...item} />}
          keyExtractor={(item) => item.id}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold',
  },

  // ? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.44,
  },
  itemImage: {
  },
  itemName: {
  },
  itemPrice: {
  },
  itemStock: {
  },
  itemButton: {
  },
  buttonText: {
  },
});
