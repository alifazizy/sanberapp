import React from "react";
import { StyleSheet, Text, View } from "react-native";

import Tugas13 from "./Tugas 13/App";
import Tugas14 from "./Tugas 14/App";
import Tugas15 from "./Tugas 15/index";
import Nav from "./Tugas 15/TugasNavigation/index";

const App = () => {
  return (
    <View style={styles.container}>
      {/* <Tugas12></Tugas12>
      <Tugas13></Tugas13>
      <Tugas14></Tugas14>
      <Tugas15></Tugas15> */}
      <Nav></Nav>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    marginTop: 32,
  },
});

export default App;