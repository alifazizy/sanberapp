import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import IonIcon from "react-native-vector-icons/Ionicons";

export default class SkillItem extends Component {
  render() {
    let skill = this.props.skill;
    return (
      <View style={styles.container}>
        <Icon name={skill.iconName} style={styles.icon} />
        <View style={styles.skillDesc}>
          <Text style={styles.skillTitle}>{skill.skillName}</Text>
          <Text style={styles.skillCategori}>{skill.categoryName}</Text>
          <Text style={styles.skillProgress}>{skill.percentageProgress}</Text>
        </View>
        <IonIcon name="ios-arrow-forward" size={75} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 12,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: "#B4E9FF",
    borderRadius: 5,
    justifyContent: "space-between",
    marginVertical: 3,
    shadowColor: "black",
    shadowOffset: { width: 0, height: 0 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 4,
  },
  skillTitle: {
    fontSize: 30,
    color: "#003366",
    fontWeight: "bold",
  },
  skillDesc: {
    flex: 1,
    paddingHorizontal: 15,
  },
  skillCategori: {
    color: "#3EC6FF",
    fontSize: 18,
  },
  skillProgress: {
    color: "white",
    fontSize: 40,
    fontWeight: "bold",
    textAlign: "right",
  },
  icon: {
    color: "#003366",
    fontSize: 90,
  },
});
