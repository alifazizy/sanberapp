import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  StatusBar
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Fa from 'react-native-vector-icons/AntDesign';
export default class App extends Component {
	render(){
	  return(
	  <View style={styles.container}>
	  <StatusBar 
    backgroundColor = {"white"}
    translucent = {false}/>
	  	<View style={styles.header}>
	  		<Image source={require('./logo.png')} style={{ width: 360, height: 55 }} />
	  	</View>
	  	<View style={styles.cheader}><Text style={{fontWeight:'bold'}}>Tentang Saya</Text></View>
	  	<View style={styles.content}>
	  		<Icon style={styles.people} name="account-circle" size={100} />
	  		<Text style={styles.identity}>Alif Abdillah Azizy</Text>
        <Text style={styles.identity3}>React Native Developer</Text>
        <View style={styles.box}>
          <Text style={styles.identity2}>Portofolio</Text>
          <Text style={{borderWidth:1,width:360,borderColor:'white',marginTop:10}}></Text>
          <View style={styles.page}>
            <View style={styles.satu}>
              <Fa name="gitlab" style={{padding:5,textAlign:'center'}} size={50} />
              <Text style={styles.identity2}>@alifazizy</Text>
            </View>
            <View style={styles.satu}>
              <Fa name="github" style={{padding:5,textAlign:'center'}} size={50} />
              <Text style={styles.identity2}>@AzyAlif</Text>
            </View>
          </View>
        </View>
        <View style={styles.box}>
          <Text style={styles.identity2}>Tentang Saya</Text>
          <Text style={{borderWidth:1,width:360,borderColor:'white',marginTop:10}}></Text>
          <View style={styles.page}>
            <View style={styles.satu}>
              <Fa name="facebook-square" style={{padding:5,textAlign:'center'}} size={50} />
              <Text style={styles.identity2}>Alif Azizy</Text>
            </View>
            <View style={styles.satu}>
              <Fa name="Instagram" style={{padding:5,textAlign:'center'}} size={50} />
              <Text style={styles.identity2}>@alif4zizy</Text>
            </View>
          </View>
        </View>
	  	</View>
	  	<View style={styles.footer}>
	  		<Text style={{fontWeight:'bold'}}>Copyright By @Student</Text>
	  	</View>
	  </View>
	  )
	}
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor:'white',
  },
  header: {
  	marginTop:50,
  	height: 120,
    backgroundColor: 'white',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center'
  },
  cheader:{
  	backgroundColor:'#57bce7',
  	flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height:55,
    width:400,
  },
  content:{
  	height:600,
  	padding:20
  },
  box:{
    marginTop:20,
    backgroundColor:'#57bce7',
    height:180,
    width:360,
    borderRadius:10,

  },
  people:{
    textAlign:'center',
    height:55,
    width:360
  },
  identity:{
    textAlign:'center',
    marginTop:60,
    color:'grey',
    fontFamily:'Bernard MT',
    color:'#036',
    backgroundColor:'./DSC_9814.jpg'
  },
  identity2:{
    textAlign:'left',
    marginTop:10,
    marginLeft:10,
    color:'grey',
    fontFamily:'Bernard MT',
    color:'#036'
  },
  identity3:{
    textAlign:'center',
    marginTop:10,
    marginLeft:10,
    color:'grey',
    fontFamily:'Bernard MT',
    color:'#036'
  },
  page:{
    width:360,
    height:150,
    padding:10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  satu:{
    width:170,
    height:150,
    padding:20,
    textAlign:'center'
  },
  footer:{
  	marginTop:50,
  	height: 60,
    backgroundColor: '#57bce7',
    elevation: 3,
    paddingHorizontal: 15,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center'
  }
});